﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using SQLite;
using Xamarin.Forms;

namespace App1
{
    public class Messages : INotifyPropertyChanged
    {
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int Id { get; set; }
        public Guid MessageId { get; set; }

        public string Message { get; set; }
        public string CharapterName { get; set; }
        public MessageType Type { get; set; }

        public int IncomingReference { get; set; }
        public int? OutcomingReference { get; set; }
        public MessageType ReferenceType { get; set; }

        public bool IsCheckPoint { get; set; } = false;

        private bool _showRestore;

        public bool ShowRestore
        {
            get { return _showRestore; }
            set
            {
                _showRestore = value;
                NotifyPropertyChanged("ShowRestore");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
            
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
                
            }
            
        }



public Messages(Guid id, string message, MessageType type, int incomingReference, int? outcomingReference,
            MessageType referenceType, bool isCheckPoint = false)
        {
            MessageId = id;
            Message = message;
            Type = type;
            IncomingReference = incomingReference;
            OutcomingReference = outcomingReference;
            IsCheckPoint = isCheckPoint;
            ReferenceType = referenceType;
            ShowRestore = false;
        }

        public Messages(Guid id, string message, MessageType type, int incomingReference, int? outcomingReference,
            MessageType referenceType, string charapterName) : 
            this(id, message, type, incomingReference, outcomingReference, referenceType, true)
        {
            CharapterName = charapterName;
        }

        public Messages()
        {

        }
    }

    public enum MessageType
    {
        User = 0,
        Bot
    }
}
