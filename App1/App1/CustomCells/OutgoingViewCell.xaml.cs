﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App1.Views;
using Xamarin.Forms;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace App1.CustomCells
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public partial class OutgoingViewCell : ViewCell
    {
        public MainPage MyParent { get; set; }

        public OutgoingViewCell()
        {
            InitializeComponent();
            
        }

        public static BindableProperty ParentBindingContextProperty = BindableProperty.Create(nameof(ParentBindingContext), typeof(object), typeof(MainPage), null);

        public object ParentBindingContext
        {
            get { return GetValue(ParentBindingContextProperty); }
            set { SetValue(ParentBindingContextProperty, value); }
        }


        public void OnRestoreCliked(object sender, EventArgs e)
        {
            //MainPage.Init
            var t = 2;
            MainPage.Instance.OnRestoreCliked(sender, e);
            
        }

    }
}
