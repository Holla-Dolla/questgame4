﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1.CustomCells
{
    class MessagesDataTemplateSelector : DataTemplateSelector
    {
        private readonly DataTemplate _incomingDataTemplate;
        private readonly DataTemplate _outgoingDataTemplate;
        private readonly DataTemplate _dieDataTemplate;

        public MessagesDataTemplateSelector()
        {
            _incomingDataTemplate = new DataTemplate(typeof(IncomingViewCell));
            _outgoingDataTemplate = new DataTemplate(typeof(OutgoingViewCell));
            _dieDataTemplate = new DataTemplate(typeof(DieViewCell));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var messages = item as Messages;

            if (messages == null)
                return null;

            if (messages.OutcomingReference == null)
                return _dieDataTemplate;
            
            //var refer =  messages.Type == MessageType.Bot ? _outgoingDataTemplate : _incomingDataTemplate;

            if (messages.Type == MessageType.Bot)
            {
                _outgoingDataTemplate.SetValue(OutgoingViewCell.BindingContextProperty, container.BindingContext);

                return _outgoingDataTemplate;
            }

            return _incomingDataTemplate;
        }
    }
}
