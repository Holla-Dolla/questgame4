﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App1.DataBase;
using App1.Interfaces;
using Xamarin.Forms;

namespace App1
{
    public partial class App : Application
    {

        public static BaseDataBase database;
        public static MessageDataBase messageDataBase;

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new Views.StartPage())
            {

            };
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public static BaseDataBase Database => database ?? (database =
                                                   new BaseDataBase(DependencyService.Get<IFileHelper>().GetLocalFilePath("CommonSQLite.db3")));

        public static MessageDataBase MessageDataBase => messageDataBase ?? (messageDataBase =
                                                             new MessageDataBase(DependencyService.Get<IFileHelper>()
                                                                 .GetLocalFilePath("CommonSQLite.db3")));
    }
}
