﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App1.Objects.Common
{
    public class CommonSettings
    {
        public string Settings { get; set; }
        public int Id { get; set; }
        public CommonType Type { get; set; }
    }
}
