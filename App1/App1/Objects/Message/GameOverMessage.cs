﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App1.Objects.Message
{
    public class GameOverMessage
    {
        public GameOverMessageType Type { get; set; }
        public string Message { get; set; }
    }

    public enum GameOverMessageType
    {
        StartNew,
        RestoreCheckPoint,
        RestoreAnyMessage //only for premium user
    }
}
