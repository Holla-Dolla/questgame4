﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using App1.Objects.Common;
using SQLite;

namespace App1.DataBase
{
    public class BaseDataBase
    {
        public readonly SQLiteAsyncConnection database;

        public BaseDataBase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<CommonSettings>().Wait();
        }

        public Task<List<CommonSettings>> GetItemsAsync()
        {
            return database.Table<CommonSettings>().ToListAsync();
        }

        public Task<CommonSettings> GetItemByIdAsync(int id)
        {
            return database.Table<CommonSettings>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public Task<CommonSettings> GetItemByTypeAsync(CommonType type)
        {
            return database.Table<CommonSettings>().Where(i => i.Type == type).FirstOrDefaultAsync();
        }
        public Task<int> SaveItemAsync(CommonSettings item)
        {
            if (item.Id != 0)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                return database.InsertAsync(item);
            }
        }

        public Task<int> DeleteItemAsync(CommonSettings item)
        {
            return database.DeleteAsync(item);
        }

        public Task<int> DeleteItemByTypeAsync(CommonType type)
        {
            var itemAsync = GetItemByTypeAsync(type);
            itemAsync.Wait();

            var item = itemAsync.Result;

            if(item == null)
                return null;

            var id =  database.DeleteAsync(item);

            return id;
        }
    }
}
