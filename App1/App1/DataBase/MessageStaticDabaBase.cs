﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using App1.Objects.Message;

namespace App1.DataBase
{
    public class MessageStaticDabaBase
    {
        private const MessageType Bot = MessageType.Bot;
        private const MessageType User = MessageType.User;

        public void FillMessages(ObservableCollection<Messages> messagesCollectio)
        {
            FillIncomingMessages(messagesCollectio);
            FillAsnwerMessages(messagesCollectio);
        }

        public void FillIncomingMessages(ObservableCollection<Messages> messagesCollection)
        {
            messagesCollection.Add(new Messages(Guid.NewGuid(), "привет, как дела?", Bot, 0, 0, User, "first"));

            messagesCollection.Add(new Messages(Guid.NewGuid(), "okey", Bot, 1, 1, User));
            messagesCollection.Add(new Messages(Guid.NewGuid(), "why so?", Bot, 2, 2, User));
            messagesCollection.Add(new Messages(Guid.NewGuid(), "fuk u too?", Bot, 3, null, User)); //end story(die)

            messagesCollection.Add(new Messages(Guid.NewGuid(), "And me Ok, But u loose  And me Ok, But u loos And me Ok, But u loos", Bot, 4, null, User));//end story(die)
            messagesCollection.Add(new Messages(Guid.NewGuid(), "u HUI, But u loose", Bot, 5, null, User));//end story(die)
            messagesCollection.Add(new Messages(Guid.NewGuid(), "Why Couse/no couse, But u loose", Bot, 6, null, User)); //end story(die)
        }

        public void FillAsnwerMessages(ObservableCollection<Messages> messagesCollection)
        {
            messagesCollection.Add(new Messages(Guid.NewGuid(), "привет, nice?", User, 0, 1, Bot));
            messagesCollection.Add(new Messages(Guid.NewGuid(), "привет, bad?", User, 0, 2, Bot));
            messagesCollection.Add(new Messages(Guid.NewGuid(), "привет, fuk?", User, 0, 3, Bot));

            messagesCollection.Add(new Messages(Guid.NewGuid(), "and u?", User, 1, 4, Bot));
            messagesCollection.Add(new Messages(Guid.NewGuid(), "hui", User, 1, 5, Bot));

            messagesCollection.Add(new Messages(Guid.NewGuid(), "couse", User, 2, 6, Bot));
            messagesCollection.Add(new Messages(Guid.NewGuid(), "no couse", User, 2, 6, Bot));
        }

        public List<GameOverMessage> GetGameOverButtonsText()
        {
            return new List<GameOverMessage>
            {
                new GameOverMessage()
                {
                    Message = "Start New Game",
                    Type = GameOverMessageType.StartNew
                },
                new GameOverMessage()
                {
                    Message = "Start From Last Charpter",
                    Type = GameOverMessageType.RestoreCheckPoint
                },
                new GameOverMessage()
                {
                    Message = "Start From Any Previous Message",
                    Type = GameOverMessageType.RestoreAnyMessage
                }
            };
        }
    }
}
