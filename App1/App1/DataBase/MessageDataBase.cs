﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using App1.Objects.Common;
using SQLite;

namespace App1.DataBase
{
    public class MessageDataBase
    {
        public readonly SQLiteAsyncConnection database;

        public MessageDataBase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<Messages>().Wait();
        }

        public Task<List<Messages>> GetItemsAsync()
        {
            return database.Table<Messages>().ToListAsync();
        }

        public List<Messages> GetItems()
        {
            var resAsync = database.Table<Messages>().ToListAsync();
            resAsync.Wait();

            return resAsync.Result;
        }

        public Task<Messages> GetItemByMessageIdAsync(Guid id)
        {
            return database.Table<Messages>().Where(i => i.MessageId == id).FirstOrDefaultAsync();
        }

        public Messages GetItemByMessageId(Guid id)
        {
            var messagesAsync = GetItemByMessageIdAsync(id);

            messagesAsync.Wait();

            return messagesAsync.Result;
        }

        public Task<Messages> GetItemByTypeAsync(MessageType type)
        {
            return database.Table<Messages>().Where(i => i.Type == type).FirstOrDefaultAsync();
        }
        public Task<int> SaveItemAsync(Messages item)
        {
            //if (item.Id != 0)
            //{
            //    return database.UpdateAsync(item);
            //}
            item.Id = 0;

            return database.InsertAsync(item);
        }

        public Task<int> DeleteItemAsync(Messages item)
        {
            return database.DeleteAsync(item);
        }

        public void DeleteItemById(Guid id)
        {
            var item = GetItemByMessageId(id);

            if(item == null)
                return;

            database.DeleteAsync(item).Wait();
        }

        public void ClearTableAsync()
        {
            database.DropTableAsync<Messages>().Wait();
            database.CreateTableAsync<Messages>();
        }

        public Task<int> DeleteItemByTypeAsync(MessageType type)
        {
            var itemAsync = GetItemByTypeAsync(type);
            itemAsync.Wait();

            var item = itemAsync.Result;

            if (item == null)
                return null;

            var id = database.DeleteAsync(item);

            return id;
        }
    }
}
