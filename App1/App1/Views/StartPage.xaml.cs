﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App1.Objects.Common;
using Xamarin.Forms;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace App1.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public partial class StartPage : ContentPage
    {
        public StartPage()
        {
            this.InitializeComponent();

            this.BindingContext = this;
            
        }

        public void NavigateTo(object sender, EventArgs e)
        {
            var dbCommonSettingAsync = App.Database.GetItemByTypeAsync(CommonType.CommonSettings);
            dbCommonSettingAsync.Wait();

            var commonSett = dbCommonSettingAsync.Result;

            switch (sender.GetType().GetProperties().First(x=> x.Name =="ClassId").GetValue(sender))
            {
                case ("Start"):
                    if (commonSett == null)//TODO
                        this.Navigation.PushAsync(new IntroPage());
                    else
                        this.Navigation.PushAsync(new MainPage());
                    break;
                case ("Settings"):
                    this.Navigation.PushAsync(new Settings());
                    break;
            }
        }

        public void ClearDB(object sender, EventArgs e)
        {
            App.Database.DeleteItemByTypeAsync(CommonType.CommonSettings);
        }
    }
}
