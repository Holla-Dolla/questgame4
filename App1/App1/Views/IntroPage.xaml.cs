﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App1.Objects.Common;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class IntroPage : ContentPage
	{
		public IntroPage ()
		{
			InitializeComponent ();
            CheckPage();
		}

	    private void SetAttempt()
	    {
	        App.Database.SaveItemAsync(new CommonSettings()
	        {
	            Id = 0,
	            Settings = "{isFirstAttempt = false}",
	            Type = CommonType.CommonSettings
	        });
	    }

	    private void CheckPage()
	    {
	        var resultAsuync = App.Database.GetItemByTypeAsync(CommonType.CommonSettings);
	        resultAsuync.Wait();

	        var result = resultAsuync.Result;

            if(result == null)
                SetAttempt();
            else
            {
                this.Navigation.PushAsync(new MainPage());
            }
	    }
	}
}