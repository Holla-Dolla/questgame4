﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;
using App1.DataBase;
using App1.Helpers;
using App1.Objects.Message;
using App1.Utils;

namespace App1.Views
{
    //TODO bug: after restore messages first message has been disappear ((
    public partial class MainPage : ContentPage
    {
        public static MainPage Instance { get; set; }
        public static string staticVar = "test";
        public ObservableCollection<Messages> staticMessages { get; set; } = new ObservableCollection<Messages>();
        public ObservableCollection<Messages> currentMessages { get; set; } = new ObservableCollection<Messages>();
        public Grid Grid;
        public MessageHelper MessageHelper = new MessageHelper();
        public MessageStaticDabaBase StaticDb = new MessageStaticDabaBase();
        //PIZDA SKOLKO TYT GAVNA
        public MainPage()
        {
            InitializeComponent();
            Instance = this;

            StaticDb.FillMessages(staticMessages);

            Grid = (Grid)StackButtons.Children.First();

            InitMessages();

            this.currentMessages.CollectionChanged += (sender, e) =>
            {
                if(currentMessages.Count > 0)
                     MessagesListView.CustScrollTo(currentMessages.Last());
            };

            this.BindingContext = this;
        }

        public void FillButtons(int? incomingReference)
        {
            if (incomingReference == null)
            {
                MessageHelper.ShowGameOverButtons(Grid, currentMessages);
                return;
            }

            var mesagess = staticMessages.GetMessagesByIncomingId(incomingReference.Value, MessageType.User);
            var i = 0;

            foreach (Button button in Grid.Children)
            {
                var message = mesagess.Count <= i ? null : mesagess[i];

                if (message != null)
                {
                    button.Text = message.Message;
                    button.ClassId = $"{message.MessageId}_{message.OutcomingReference?.ToString() ?? "null" }";
                }
                else
                {
                    button.IsVisible = false;
                }

                i++;
            }
        }

        public void InitMessages()
        {
            var messagess = App.MessageDataBase.GetItems();

            messagess.All(x => x.ShowRestore = false);

            staticMessages.SetShowRestore(false);

            if (messagess.Count == 0)
            {
                //TODO move to method
                var outcomingReference = staticMessages.First().OutcomingReference;

                if (outcomingReference != null)
                    FillButtons(outcomingReference.Value);

                currentMessages.AddAndSave(staticMessages.First());

                currentMessages.SetShowRestore(false);

                return;
            }

            foreach (var message in messagess)
            {
                if(currentMessages.Any(mess => mess.MessageId == message.MessageId))
                    continue;

                currentMessages.Add(message);
            }

            var outcomingReference2 = currentMessages.LastOrDefault(x=> x.Type == MessageType.Bot)?.OutcomingReference;

           // currentMessages.SetShowRestore(false);

            if (outcomingReference2 != null)
                FillButtons(outcomingReference2.Value);
            else
                MessageHelper.ShowGameOverButtons(Grid, currentMessages);

            MessagesListView.CustScrollTo(currentMessages.Last());
        }

        public async void OnGameOverClicked(object sender, EventArgs e)
        {
            var classId = ((Button)sender).ClassId;

            if (classId.Contains(GameOverMessageType.StartNew.ToString()))
            {
                var answer = await DisplayAlert(null, "Would you like to start a new game", "Yes", "No");

                if (answer)
                {
                    MessageHelper.StartNewGame(currentMessages);
                    InitMessages();
                }

                return;
            }

            if (classId.Contains(GameOverMessageType.RestoreCheckPoint.ToString()))
            {
                var charapters = currentMessages.GetCharaptersMessages();

                var action = await DisplayActionSheet("Please, choose charapter!",
                    "Cancel",null, charapters.Select(x=> x.CharapterName).ToArray());
               
                if(action == "Cancel")
                    return;

                MessageHelper.RestoreCheckPoint(action, currentMessages);
                InitMessages();
                return;
            }

            //Todo check for PA
            //TODO think about text
           await DisplayAlert(null, "To start from any message, choose it", "Ok");
        }

        public void OnAnswerClicked(object sender, EventArgs e)
        {
            var refIdString = ((Button)sender).ClassId;

            if (refIdString.Contains("Game_Over"))
            {
                OnGameOverClicked(sender, e);
                return;
            }

            var refIds = refIdString.Split('_').ToList();

            if (refIds[1] == "null")
            {
                MessageHelper.ShowGameOverButtons(Grid, currentMessages);

                OnPropertyChanged();

                MessagesListView.CustScrollTo(currentMessages.Last());
                return;
            }

            var messagesLocal = new List<Messages> { staticMessages.First(x => x.MessageId == Guid.Parse(refIds[0])) };

            messagesLocal.AddRange(staticMessages.GetMessagesByIncomingId(int.Parse(refIds[1]), MessageType.Bot));

            foreach (var message in messagesLocal)
            {
                currentMessages.AddAndSave(message);
            }

            MessagesListView.CustScrollTo(currentMessages.Last());

            FillButtons(currentMessages.Last().OutcomingReference);
        }

        public void OnRestoreCliked(object sender, EventArgs e)
        {
            //MainPage.Init
            var t = 2;
        }

        void MyListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            MessagesListView.SelectedItem = null;

            App.MessageDataBase.ClearTableAsync();
        }

        void MyListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            MessagesListView.SelectedItem = null;
        }
    }
}