﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using App1.DataBase;
using App1.Objects.Message;
using App1.Utils;
using App1.Views;
using Xamarin.Forms;

namespace App1.Helpers
{
    public class MessageHelper
    {
        public void RemoveButtons(Grid grid)
        {
            foreach (Button button in grid.Children)
            {
                button.IsVisible = false;
            }
        }

        public void ShowGameOverButtons(Grid grid, ObservableCollection<Messages> currentMessages)
        {
            currentMessages.SetShowRestore(true);

            var mess = new MessageStaticDabaBase().GetGameOverButtonsText();

            var i = 0;

            foreach (Button button in grid.Children)
            {
                button.Text = mess[i].Message;
                button.ClassId = $"Game_Over_{mess[i].Type.ToString()}";
                button.IsVisible = true;
                i++;
            }
        }

        public void StartNewGame(ObservableCollection<Messages> currentMessages)
        {
            currentMessages.RemoveAllMessages();
        }

        public void RestoreCheckPoint(string action, ObservableCollection<Messages> currentMessages)
        {
            var checkPointMessage = currentMessages.GetCharaptersMessages().FirstOrDefault(mess => mess.CharapterName == action);

            if(checkPointMessage == null)
                return;

            currentMessages.RemoveAfterMessage(checkPointMessage);

            currentMessages.SetShowRestore(false);
        }

        public void RestoreMessage()
        {
            
               
        }
    }
}
