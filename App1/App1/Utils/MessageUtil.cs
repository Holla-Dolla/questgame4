﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using App1.Views;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace App1.Utils
{
    public static class MessageUtil
    {
        public static List<Messages> GetMessagesByIncomingId(this ObservableCollection<Messages> messagesCollection, int incomingReference, MessageType type)
        {
            return messagesCollection.Where(x => x.IncomingReference == incomingReference && x.Type == type).ToList();
        }

        public static void AddAndSave(this ObservableCollection<Messages> whereToAdd, Messages whatToAdd)
        {
            try
            {
                if(whereToAdd == null || whatToAdd == null)
                    throw new Exception("what To Add must contain message. method: MessageUtil.AddAndSave()");

                whereToAdd.Add(whatToAdd);
                App.MessageDataBase.SaveItemAsync(whatToAdd);
            }
            catch (Exception e)
            {
                throw new Exception(JsonConvert.SerializeObject(whatToAdd),e);
            }
        }

        public static void CustScrollTo(this ListView lw, object target)
        {
            lw.ScrollTo(target, ScrollToPosition.MakeVisible, true);
        }

        public static void RemoveAllMessages(this ObservableCollection<Messages> messages)
        {
            try
            {
                while (messages.Count > 0)
                {
                    App.MessageDataBase.DeleteItemById(messages[0].MessageId);
                    messages.RemoveAt(0);
                }
            }
            catch (Exception e)
            {
                //TODO add error handling
            }
        }

        public static void RemoveAfterMessage(this ObservableCollection<Messages> messages, Messages message)
        {
            try
            {
                var indexFrom = messages.IndexOf(message);

                for (var i = messages.Count - 1; i > indexFrom; i--)
                {
                    App.MessageDataBase.DeleteItemById(messages[i].MessageId);
                    messages.RemoveAt(i);
                }
            }
            catch (Exception e)
            {
                //TODO add error handling
            }
        }

        public static List<Messages> GetCharaptersMessages(this ObservableCollection<Messages> messages)
        {
            return messages.Where(mess => mess.IsCheckPoint).ToList();
        }

        public static void SetShowRestore(this ObservableCollection<Messages> messages, bool showRestore)
        {
            foreach (var m in messages)
            {
                m.ShowRestore = showRestore;
            }

        }
    }
}
